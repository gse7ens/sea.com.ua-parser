import requests
from bs4 import BeautifulSoup
import csv


CSV = "components.csv"
HOST = "https://www.sea.com.ua"
url = input("Введите адрес для парсинга: ")
urls = []

HEADERS = {
    "accept": "text/html,application/xhtml+xml,application/xml;q=0.9,image/avif,image/webp,image/apng,*/*;q=0.8,application/signed-exchange;v=b3;q=0.7",
    "user-agent": "Mozilla/5.0 (Windows NT 10.0; Win64; x64) AppleWebKit/537.36 (KHTML, like Gecko) Chrome/120.0.0.0 Safari/537.36"
}


def get_html(url):
    response = requests.get(url, headers=HEADERS)
    return response


def main_title(html):
    soup = BeautifulSoup(html, "html.parser")
    title = soup.find("h1", class_="main-title").text
    return title


# title = main_title(html)

def get_content(html):
    soup = BeautifulSoup(html, "html.parser")
    span_tags = soup.find_all("span", class_="count-range")
    for span_tag in span_tags:
        span_tag.clear() # clearing not necessary span tags
    items = soup.find("div", class_="product-list").find_all("div", class_="item")
    components = []
    for item in items:
        components.append(
            {
                "title": item.find("div", class_="item-title").get_text().strip(),
                "price": item.select('div[class*="price"]')[0].get_text().strip().replace(" грн",""),
                "in_store": item.find("div", class_="quantity").div.get_text().replace("На складі: ","").replace(" шт.",""),
                "description": item.find("div", class_="description").get_text().strip(),
                "url": "No address" if item.find("div", class_="item-title").find("a") == None else
                HOST + item.find("div", class_="item-title").find("a").get("href")
            }
        )
    return components


# html = get_html(url).text
# print(get_content(html))

def get_urls():
    last_page = input("Введите кол-во страниц для парсинга: ")
    page = int(last_page)
    x = 0
    while x < page:
        x += 1
        urls.append(url + "?page=" + str(x))
    return last_page


def save_doc(components, path):
    with open(path, "w", newline="") as file:
        writer = csv.writer(file, delimiter=";")
        writer.writerow(["title", "price", "in_store", "description", "url"])
        for item in components:
            writer.writerow([item["title"], item["price"], item["in_store"], item["description"], item["url"]])


html = get_html(url).text
last_page = get_urls()
# main_title(html)

components = []
for url in urls:
    print("Parsing " + url)
    html = get_html(url)
    components.extend(get_content(html.text))


save_doc(components, CSV)

k = input("Нажмите Enter для выхода: ")